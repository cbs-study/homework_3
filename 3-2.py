# Створіть 2 класи мови, наприклад, англійська та іспанська. В обох класів має бути метод greeting().
# Обидва створюють різні привітання. Створіть два відповідні об'єкти з двох класів вище та
# викличте дії цих двох об'єктів в одній функції (функція hello_friend).


class English:
    def greeting(self):
        return f"Hi, how are you?"


class Spanish:
    def greeting(self):
        return "¿Hola, cómo estás?"


def hello_friend():
    english = English()
    spanish = Spanish()

    print("Greetings in English:", english.greeting())
    print("Greetings in Spanish:", spanish.greeting())


hello_friend()
