# Використовуючи посилання наприкінці цього уроку, ознайомтеся з таким засобом інкапсуляції, як властивості.
# Ознайомтеся з декоратором property у Python. Створіть клас, що описує температуру і дозволяє задавати та отримувати
# температуру за шкалою Цельсія та Фаренгейта, причому дані можуть бути задані в одній шкалі, а отримані в іншій.


class Temperature:
    def __init__(self, celsius):
        self._celsius = celsius

    @property
    def celsius(self):
        return self._celsius

    @celsius.setter
    def celsius(self, value):
        self._celsius = value

    @property
    def fahrenheit(self):
        return self._celsius * 9 / 5 + 32

    @fahrenheit.setter
    def fahrenheit(self, value):
        self._celsius = (value - 32) * 5 / 9


temp = Temperature(20)

temp.celsius = 30
print("Temperature in Celsius:", int(temp.celsius))
print("Temperature in Fahrenheit:", int(temp.fahrenheit))

temp.fahrenheit = 75
print("Temperature in Celsius after setting Fahrenheit:", int(temp.celsius))
print("Temperature in Fahrenheit after setting Fahrenheit:", int(temp.fahrenheit))
