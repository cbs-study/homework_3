# Створіть клас, який описує автомобіль. Які атрибути та методи мають бути повністю інкапсульовані?
# Доступ до таких атрибутів та зміну даних реалізуйте через спеціальні методи (get, set).


class Car:
    def __init__(self, make, model, year, num_doors):
        self.__make = make
        self.__model = model
        self.__year = year
        self.__num_doors = num_doors

    def get_make(self):
        return self.__make

    def set_make(self, make):
        self.__make = make

    def get_model(self):
        return self.__model

    def set_model(self, model):
        self.__model = model

    def get_year(self):
        return self.__year

    def set_year(self, year):
        self.__year = year

    def get_num_doors(self):
        return self.__num_doors

    def set_num_doors(self, num_doors):
        self.__num_doors = num_doors


car1 = Car("Mercedes-Benz", "E-Class", 2017, 4)

print(car1.get_make())
print(car1.get_model())
